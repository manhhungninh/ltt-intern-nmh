

<?php
class ExerciseString {
    public $check1;
    public $check2;
    
    function writeFile($check1,$check2,$count) {
        $myfile = fopen('result_file.txt', 'w') or die('Unable to open file!');
        if ($check1 === false) {
            fwrite($myfile, "check1 là chuỗi không hợp lệ.\n");
        } else {
            fwrite($myfile, "check1 là chuỗi hợp lệ.\n"); 
        }
        if ($check2 === false) {
            fwrite($myfile, 'check2 là chuỗi không hợp lệ. Và có ' .$count. " câu\n");
        } else {
            fwrite($myfile, 'check2 là chuỗi hợp lệ. Và có ' .$count. " câu\n"); 
        }
    }

    function customReadFile ($file) {
        $myfile = fopen($file, 'r') or die('Unable to open file!');
        $content = fread($myfile,filesize($file));
        fclose($myfile);
        return $content;
    }   
    
    function checkValidString ($content){
        global $result;
        $result = substr_count($content, '.');
        if (!strpos($content, 'book') === false || !(strpos($content, 'restaurant')) === false ) {
            $check = true;
        }
        else {
            $check = false;
        }
        return $check;
    }
}
$object1 = new ExerciseString();
$object1->check1 = $object1->checkValidString($object1->customReadFile('file1.txt'));
$object1->check2 = $object1->checkValidString($object1->customReadFile('file2.txt'));
$result02 = $GLOBALS['result'];
$object1->writeFile($object1->check1,$object1->check2,$result02);
?>
